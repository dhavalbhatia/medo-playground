// importing(requiring) module and getting assert from chai
// an assertion library
var assert = require('chai').assert;

// getting start() from src -> backend -> lib -> app.ts
var first = require('../backend/lib/app');

// getting readData() from src -> backend -> lib -> dataSource.ts
var second = require('../backend/lib/dataSource')

// describe ends up as global in test directory
// unit test 1
describe("Simple test to check port number", function() {
    it('port number should be 8080', function()  {
        assert.equal(first(), '8080');
    });
});

// unit test 2
describe("Read data test", function() {
    it('verifying if jim exists in file', function() {
        assert.equal(second(), 'jim');
    });
});