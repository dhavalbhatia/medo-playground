import Logger, { Log } from "./logger";
export type EntityType = "patient";

// QUESTION: Why have different implementations of DataSource?
// To ensure proper working in different browsers

// QUESTION: What do abstract, private, protected, public, async mean?
// abstract(keyword for declaring classes or methods) classes- made for creating planned inheritances
// (abstract methods are declared by signature in super classes)
// private(keyword that is access modifer methods/classes)
// protected(keyword)- can be used in same classes or sub classes
// async(keyword)- not line by line execution and returns a promise
export default abstract class DataSource {
    protected readonly log: Log;

    constructor(logName: string) {
        this.log = Logger.getLog(logName);
    }
    // Perform any required data structure intialization
    public abstract initialize(): Promise<void>;

    public abstract readAll(type: EntityType): Promise<any[]>;
    public abstract saveData(type: EntityType, id: string, data: any): Promise<void>;
    public abstract deleteData(type: EntityType, id: string): Promise<void>;

    public async clearAll(type: EntityType) {
        const all = await this.readAll(type);
        for (const data of all) {
            await this.deleteData(type, data.id);
        }
    }

    public async readData(type: EntityType, id: string) {
        const all = await this.readAll(type);
        for (const data of all) {
            if (data.id === id) {
                return data;
            }
        }
        return null;
    }
}