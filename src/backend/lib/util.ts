import Path from "path";
import fs from "fs";
import crypto from "crypto";

// QUESTION: Why use Path here?
// path is a native utility model
// provides utilities for working with file and directory paths
// process.cwd() gets current working directory(better than hard coding)
const PUBLIC_KEY_PATH = Path.join(process.cwd(), "certs", "public.crt");
const PRIVATE_KEY_PATH = Path.join(process.cwd(), "certs", "private.key");

// QUESTION: Can we use aynchronous read?
// No, since these are global constants. And, many other features would fail without these
const PUBLIC_KEY = fs.readFileSync(PUBLIC_KEY_PATH);
const PRIVATE_KEY = fs.readFileSync(PRIVATE_KEY_PATH);

export default {
    decrypt: async (base64Enc: string) => {
        // QUESTION: What are these encodings for?
        // Buffers take a string and perform base-64 encoding of the string
        const decrypted = crypto.publicDecrypt(PUBLIC_KEY,
            Buffer.from(base64Enc, "base64")).toString("utf8");
        return decrypted;
    },

    encrypt: async (plain: string) => {
        return crypto.privateEncrypt(PRIVATE_KEY,
            Buffer.from(plain, "utf8")).toString("base64");
    },

    generateRandom: (length: number) => {
        return crypto.pseudoRandomBytes(length).toString("base64");
    }
};