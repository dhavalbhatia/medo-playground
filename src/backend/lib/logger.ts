import $ from "jquery";

const LOGGERS: { [key: string]: Log } = {};
export class Log {
    constructor(private name: string) {
    }

    public info(...args: any) {
        this.log("info", args);
    }

    public error(...args: any) {
        this.log("error", args);
    }


    private log(level: "info" | "error", args: []) {
        // TODO timestamps, formatting
        // time stamps added and formatted
        
        // tslint:disable-next-line
        // npm install --save jquery(done)
        // temporarily case $ as any
        //delete ($ as any).summernote.options.keyMap.pc.TAB
        console.log.apply(null, [level.toUpperCase(), `[${this.name}]`, ...args]);
    }
}

export default {
    getLog: (name: string): Log => {
        if (!LOGGERS[name]) {
            LOGGERS[name] = new Log(name);
        }
        return LOGGERS[name];
    }
};